package course;

import competitor.Competitor;

public abstract class Obstacle {
    public abstract void doIt(Competitor competitor);
}

