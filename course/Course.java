package course;

import competitor.Competitor;
import competitor.Team;


public class Course {
    private Obstacle[] obstacles;

    public Course(Cross cross, Wall wall, Water water, Obstacle... obstacles) {
        this.obstacles = obstacles;
    }

    public void doIt(Team team) {
        for (Competitor c : team.getMembers()) {
            for (Obstacle o : obstacles) {
                o.doIt(c);
                if (!c.isOnDistance()) break;
            }
        }
    }
}
