import competitor.*;
import course.Course;
import course.Cross;
import course.Wall;
import course.Water;

public class Main {
    public static void main(String[] args) {
//        Competitor[] competitors = {new Human("Боб"), new Cat("Барсик"), new Dog("Бобик")};
//        Obstracle[] course = {new Cross(80), new Wall(2), new Wall(1), new Cross(120)};
//        for (Competitor c : competitors) {
//            for (Obstracle o : course) {
//                o.doIt(c);
//                if (!c.isOnDistance()) break;
//            }
//        }
//        for (Competitor c : competitors) {
//            c.info();
//        }
//        System.out.println("END!");

        Team team = new Team("Команда1", new Human("Bob"), new Cat("Vaska"), new Dog("Bobick"));
        Course course = new Course(new Cross(80), new Wall(2), new Water(1));
        course.doIt(team);
        team.showWinners();


    }
}
